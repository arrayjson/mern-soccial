const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");

//Load Model
const PostModel = require("../../models/Post");
const profileModel = require("../../models/ProfileModel");

// Validation
const validatePostInput = require("../../validation/post");

// @router GET api/posts/test
// @desc Test posts route
// @access public
router.get("/test", (req, res) => res.json({ msg: "post works" }));

// @router POST api/posts
// @desc Create posts
// @access private
router.post(
	"/",
	passport.authenticate("jwt", { session: false }),
	(req, res) => {
		const { errors, isValid } = validatePostInput(req.body);
		if (!isValid) {
			return res.status(400).json(errors);
		}

		const post = new PostModel({
			text: req.body.text,
			name: req.body.name,
			avatar: req.body.avatar,
			user: req.user.id,
		});
		post.save()
			.then((post) => res.json(post))
			.catch((err) => console.log(err));
	}
);

// @router GET api/posts
// @desc Get posts
// @access public
router.get("/", (req, res) => {
	PostModel.find()
		.sort({ date: -1 })
		.then((post) => res.json(post))
		.catch((err) => res.status(404).json({ msg: "no Post Found" }));
});

// @router GET api/posts/:id
// @desc Get special posts
// @access public
router.get("/:id", (req, res) => {
	PostModel.findById(req.params.id)
		.then((post) => res.json(post))
		.catch((err) => res.status(404).json({ msg: "no Post Found" }));
});

// @router DELETE api/posts/:id
// @desc delete post
// @access private
router.delete(
	"/:id",
	passport.authenticate("jwt", { session: false }),
	(req, res) => {
		profileModel
			.findOne({ user: req.user.id })
			.then((profile) => {
				PostModel.findById(req.params.id)
					.then((post) => {
						if (post.user.toString() !== req.user.id) {
							return res
								.status(401)
								.json({ msg: "User is not authorized " });
						}
						post.remove().then(() => res.json({ status: true }));
					})
					.catch((err) =>
						res.status(404).json({ msg: "no Post Found" })
					);
			})
			.catch((err) => res.status(404).json({ msg: "no Post Found" }));
	}
);

// @router POST api/posts/like/:id
// @desc like post
// @access private
router.post(
	"/like/:id",
	passport.authenticate("jwt", { session: false }),
	(req, res) => {
		profileModel
			.findOne({ user: req.user.id })
			.then((profile) => {
				PostModel.findById(req.params.id)
					.then((post) => {
						if (
							post.likes.filter(
								(like) => like.user.toString() == req.user.id
							).length > 0
						) {
							return res
								.status(400)
								.json({ msg: "User already like this." });
						}
						//Add user id to like array
						post.likes.unshift({ user: req.user.id });

						post.save()
							.then((post) => res.json(post))
							.catch((err) => console.log(err));
					})
					.catch((err) => {
						return res
							.status(404)
							.json({ msg: "No Post Found related to post id." });
					});
			})
			.catch((err) => {
				return res
					.status(404)
					.json({ msg: "no Post Found of current user" });
			});
	}
);

// @router POST api/posts/like/:id
// @desc like post
// @access private
router.post(
	"/unlike/:id",
	passport.authenticate("jwt", { session: false }),
	(req, res) => {
		profileModel
			.findOne({ user: req.user.id })
			.then((profile) => {
				PostModel.findById(req.params.id)
					.then((post) => {
						if (
							post.likes.filter(
								(like) => like.user.toString() == req.user.id
							).length === 0
						) {
							return res
								.status(400)
								.json({ msg: "You have not liked yet." });
						}
						//Get removed index
						const removeIndex = post.likes
							.map((item) => item.user.toString())
							.indexOf(req.user.id);
						//splice out array
						post.likes.splice(removeIndex, 1);

						//save
						post.save()
							.then((post) => res.json(post))
							.catch((err) => console.log(err));
					})
					.catch((err) => {
						return res
							.status(404)
							.json({ msg: "No Post Found related to post id." });
					});
			})
			.catch((err) => {
				return res
					.status(404)
					.json({ msg: "no Post Found of current user" });
			});
	}
);

// @router POST api/posts/comment/:id
// @desc add comment
// @access private
router.post(
	"/comment/:id",
	passport.authenticate("jwt", { session: false }),
	(req, res) => {
		const { errors, isValid } = validatePostInput(req.body);
		if (!isValid) {
			return res.status(400).json(errors);
		}

		PostModel.findById(req.params.id)
			.then((post) => {
				const newComment = {
					text: req.body.text,
					name: req.body.name,
					avatar: req.body.avatar,
					user: req.user.id,
				};

				//add to comment array
				post.comments.unshift(newComment);
				// save
				post.save()
					.then((post) => res.json(post))
					.catch((err) => console.log(err));
			})
			.catch((err) => res.status("404").json({ msg: " No post found" }));
	}
);

// @router DELETE api/posts/comment/:id/:commentId
// @desc delete comment
// @access private
router.delete(
	"/comment/:id/:commentId",
	passport.authenticate("jwt", { session: false }),
	(req, res) => {
		PostModel.findById(req.params.id)
			.then((post) => {
				if (
					post.comments.filter(
						(comment) =>
							comment._id.toString() === req.params.commentId
					).length === 0
				) {
					return res
						.status(404)
						.json({ msg: "Comment do not exists" });
				}
				const removeIndex = post.comments
					.map((item) => item._id.toString())
					.indexOf(re.params.commentId);

				//delete
				post.comments.splice(removeIndex, 1);

				// save
				post.save()
					.then((comment) => res.json(comment))
					.catch((err) => console.log(err));
			})
			.catch((err) => res.status("404").json({ msg: " No post found" }));
	}
);
module.exports = router;
