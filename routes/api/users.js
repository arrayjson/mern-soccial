const express = require("express");
const router = express.Router();
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");

// load user model
const UserModel = require("../../models/UserModel");
const passport = require("passport");

// @router POST api/users/test
// @desc Test users route
// @access public
router.post("/register", (req, res) => {
	// check validation
	const { errors, isValid } = validateRegisterInput(req.body);
	if (!isValid) {
		res.status(400).json(errors);
	}
	UserModel.findOne({ email: req.body.email }).then((user) => {
		if (user) {
			errors.msg = "Email already Exists";
			return res.status(400).json(errors);
		} else {
			const avatar = gravatar.url(req.body.email, {
				s: "200",
				r: "pg",
				d: "mm",
			});

			const newUser = UserModel({
				name: req.body.name,
				email: req.body.email,
				avatar,
				password: req.body.password,
			});

			bcrypt.genSalt(10, (err, salt) => {
				bcrypt.hash(newUser.password, salt, (err, hash) => {
					if (err) throw err;
					newUser.password = hash;
					newUser
						.save()
						.then((user) => res.json(user))
						.catch((er) => console.log(er));
				});
			});
		}
	});
});

// @router POST api/users/login
// @desc Login user /returning JWT token
// @access public
router.post("/login", (req, res) => {
	// check validation
	const { errors, isValid } = validateLoginInput(req.body);
	if (!isValid) {
		res.status(400).json(errors);
	}
	const email = req.body.email;
	const password = req.body.password;

	// find user by email
	UserModel.findOne({ email }).then((user) => {
		if (!user) {
			errors.msg = "User do not match";
			return res.status(400).json(errors);
		}
		bcrypt.compare(password, user.password).then((isMatch) => {
			if (!isMatch) {
				errors.msg = "Password mis match";
				return res.status(400).json(errors);
			} else {
				const payload = {
					id: user._id,
					name: user.name,
					email: user.email,
					avatar: user.avatar,
				};
				jwt.sign(
					payload,
					keys.secretOrKey,
					{ expiresIn: 3600 },
					(error, token) => {
						res.json({
							success: true,
							token: "Bearer " + token,
						});
					}
				);
			}
		});
	});
});

// @router POST api/users/current
// @desc Return current user
// @access private
router.get(
	"/current",
	passport.authenticate("jwt", { session: false }),
	(req, res) => {
		res.json({
			id: req.user.id,
			name: req.user.name,
			email: req.user.email,
		});
	}
);

module.exports = router;
